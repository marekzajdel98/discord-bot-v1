import discord
import asyncio
import youtube_dl
from discord.ext import commands
from itertools import cycle

TOKEN = "Your token"

client = commands.Bot(command_prefix='?')
client.remove_command("help")
status = ["Doki Doki Liteature Club", "Meow Meow", "Release me!"]

players = {}
queues = {}


def check_queue(id):
    if queues[id] != []:
        player = queues[id].pop(0)
        players[id] = player
        player.start()


async def change_status():
    await client.wait_until_ready()
    msgs = cycle(status)

    while not client.is_closed:
        current_status = next(msgs)
        await client.change_presence(game=discord.Game(name=current_status))
        await asyncio.sleep(10)


@client.event
async def on_ready():
    await client.change_presence(game=discord.Game(name="Doki Doki Literature Club"))
    print("Bot is ready.")


@client.event
async def on_message(message):
    print("{} {}".format(message.author, message.content))
    await client.process_commands(message)


@client.command()
async def ping():
    """ Sample command """
    await client.say("Pong!")


@client.command()
async def displayembed():
    """ Test command to display sample embed"""
    embed = discord.Embed(
        title="Title",
        description="This is a description.",
        colour=discord.Colour.blue()
    )
    embed.set_footer(text="This is a footer.")
    embed.set_image(url="http://isobap.com/gallery2/d/38751-2/Sparkel+Art.JPG")
    embed.set_thumbnail(url="http://isobap.com/gallery2/d/38751-2/Sparkel+Art.JPG")
    embed.set_author(name="Author Name",
                     icon_url="http://isobap.com/gallery2/d/38751-2/Sparkel+Art.JPG")
    embed.add_field(name="Field Name", value="Field Value", inline=False)
    embed.add_field(name="Field Name", value="Field Value", inline=True)
    embed.add_field(name="Field Name", value="Field Value", inline=True)
    await client.say(embed=embed)


@client.command(pass_context=True)
async def help(ctx):
    """ Sends a help message with all the commands to the requester """
    author = ctx.message.author

    embed = discord.Embed(
        colour = discord.Colour.orange()
    )

    embed.set_author(name="Help")
    embed.add_field(name=".ping", value="Returns Pong!", inline=False)

    await client.send_message(author, embed=embed)


@client.command(pass_context=True)
async def join(ctx):
    """ Bot joins the voice channel"""
    channel = ctx.message.author.voice.voice_channel
    try:
        await client.join_voice_channel(channel)
    except:
        pass


@client.command(pass_context=True)
async def leave(ctx):
    """ Bot leaves the voice channel"""
    server = ctx.message.server
    voice_client = client.voice_client_in(server)
    try:
        await voice_client.disconnect()
    except:
        pass


@client.command(pass_context=True)
async def play(ctx, url):
    """ Youtube streaming """
    server = ctx.message.server
    voice_client = client.voice_client_in(server)
    player = await voice_client.create_ytdl_player(url, after=lambda: check_queue(server.id))
    players[server.id] = player
    player.start()


@client.command(pass_context=True)
async def pause(ctx):
    """ Pauses the music """
    id = ctx.message.server.id
    players[id].pause()


@client.command(pass_context=True)
async def stop(ctx):
    """ Stops the music """
    id = ctx.message.server.id
    players[id].stop()


@client.command(pass_context=True)
async def resume(ctx):
    """ Resumes the music """
    id = ctx.message.server.id
    players[id].resume()


@client.command(pass_context=True)
async def queue(ctx, url):
    """ Puts a next song in queue """
    server = ctx.message.server
    voice_client = client.voice_client_in(server)
    player = await voice_client.create_ytdl_player(url)

    if server.id in queues:
        queues[server.id].append(player)
    else:
        queues[server.id] = [player]
    await client.say("Song queued.")


@client.command(pass_context=True)
async def skip(ctx):
    """ Skips the song """
    id = ctx.message.server.id
    players[id].stop()


client.loop.create_task(change_status())
client.run(TOKEN)
